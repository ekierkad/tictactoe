package com.example.tictactoe

import android.util.Log

class Move (array : Array<Array<Sign>>, private var mySign: Sign, private var enemySign: Sign, var moveX: Int,
            var moveY: Int
) {

    private var board = array
    var value = 0

    fun evaluate() {
        countRows()
        countColumns()
        countTopLeftDiagonals()
        countTopRightDiagonals()
        Log.d("Bot debug","Ruch $moveX $moveY ma wartosc $value")
    }

    private fun evaluateResult(myFields : Int, enemyFields : Int) : Int {
        if (myFields == 4) {
            return 100000000
        }
        if (myFields == 3 && enemyFields == 0) {
            return 500000
        }
        if (enemyFields == 2 && myFields > 0) {
            return 1000
        }
        if (enemyFields == 3 && myFields > 0) {
            return 700000
        }
        return myFields * 200 - 100 * enemyFields
    }

    private fun countRows() {
        val startingPositions = arrayOf(
            arrayOf(0,0), arrayOf(0,1),
            arrayOf(1,0), arrayOf(1,1),
            arrayOf(2,0), arrayOf(2,1),
            arrayOf(3,0), arrayOf(3,1),
            arrayOf(4,0), arrayOf(4,1))
        for (pos in startingPositions) {
            var myFields = 0
            var enemyFields = 0
            var neutralFields = 0
            for (i in 0..3) {
                when {
                    board[pos[0]][pos[1]+i] == mySign -> myFields++
                    board[pos[0]][pos[1]+i] == enemySign -> enemyFields++
                    else -> neutralFields++
                }
            }
            value += evaluateResult(myFields, enemyFields)
        }
    }

    private fun countColumns() {
        val startingPositions = arrayOf(
            arrayOf(0,0), arrayOf(1,0),
            arrayOf(0,1), arrayOf(1,1),
            arrayOf(0,2), arrayOf(1,2),
            arrayOf(0,3), arrayOf(1,3),
            arrayOf(0,4), arrayOf(1,4))
        for (pos in startingPositions) {
            var myFields = 0
            var enemyFields = 0
            var neutralFields = 0
            for (i in 0..3) {
                when {
                    board[pos[0]+i][pos[1]] == mySign -> myFields++
                    board[pos[0]+i][pos[1]] == enemySign -> enemyFields++
                    else -> neutralFields++
                }
            }
            value += evaluateResult(myFields, enemyFields)
        }
    }

    private fun countTopLeftDiagonals() {
        val startingPositions = arrayOf( arrayOf(0,0), arrayOf(1,1), arrayOf(0,1), arrayOf(1, 0))
        for (pos in startingPositions) {
            var myFields = 0
            var enemyFields = 0
            var neutralFields = 0
            for (i in 0..3) {
                when {
                    board[pos[0]+i][pos[1]+i] == mySign -> myFields++
                    board[pos[0]+i][pos[1]+i] == enemySign -> enemyFields++
                    else -> neutralFields++
                }
            }
            value += evaluateResult(myFields, enemyFields)
        }
    }

    private fun countTopRightDiagonals() {
        val startingPositions = arrayOf( arrayOf(0,4), arrayOf(0,3), arrayOf(1,4), arrayOf(1,3))
        for (pos in startingPositions) {
            var myFields = 0
            var enemyFields = 0
            var neutralFields = 0
            for (i in 0..3) {
                when {
                    board[pos[0]+i][pos[1]-i] == mySign -> myFields++
                    board[pos[0]+i][pos[1]-i] == enemySign -> enemyFields++
                    else -> neutralFields++
                }
            }
            value += evaluateResult(myFields, enemyFields)
        }
    }


}