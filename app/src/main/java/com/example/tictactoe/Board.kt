package com.example.tictactoe

import android.view.View

class Board {

    private val boardSize = 5
    var buttonsArray = Array(boardSize) { IntArray(boardSize) {0} }
    var boardArray = Array(boardSize) { Array(boardSize) {Sign.EMPTY} }


    fun initButtonsArray() {
        buttonsArray[0][0] = R.id.button00
        buttonsArray[0][1] = R.id.button01
        buttonsArray[0][2] = R.id.button02
        buttonsArray[0][3] = R.id.button03
        buttonsArray[0][4] = R.id.button04

        buttonsArray[1][0] = R.id.button10
        buttonsArray[1][1] = R.id.button11
        buttonsArray[1][2] = R.id.button12
        buttonsArray[1][3] = R.id.button13
        buttonsArray[1][4] = R.id.button14

        buttonsArray[2][0] = R.id.button20
        buttonsArray[2][1] = R.id.button21
        buttonsArray[2][2] = R.id.button22
        buttonsArray[2][3] = R.id.button23
        buttonsArray[2][4] = R.id.button24

        buttonsArray[3][0] = R.id.button30
        buttonsArray[3][1] = R.id.button31
        buttonsArray[3][2] = R.id.button32
        buttonsArray[3][3] = R.id.button33
        buttonsArray[3][4] = R.id.button34

        buttonsArray[4][0] = R.id.button40
        buttonsArray[4][1] = R.id.button41
        buttonsArray[4][2] = R.id.button42
        buttonsArray[4][3] = R.id.button43
        buttonsArray[4][4] = R.id.button44
    }

    fun getFieldCoordinates(view: View) : IntArray {
        val arr = IntArray(2)
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                if (view.id == buttonsArray[i][j]) {
                    arr[0] = i
                    arr[1] = j
                    return arr
                }
            }
        }
        return arr
    }

    fun resetBoard() {
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                boardArray[i][j] = Sign.EMPTY
            }
        }
    }

}

