package com.example.tictactoe

class Judge (private var board: Board) {

    fun findWinner(): Sign {
        var result = checkColumns()
        if (result != Sign.EMPTY) {
            return result
        }

        result = checkRows()
        if (result != Sign.EMPTY) {
            return result
        }

        result = checkTopLeftDiagonals()
        if (result != Sign.EMPTY) {
            return result
        }

        result = checkTopRightDiagonals()
        if (result != Sign.EMPTY) {
            return result
        }

        return Sign.EMPTY
    }


    private fun checkRows(): Sign {
        val startingPositions = arrayOf(
            arrayOf(0,0), arrayOf(0,1),
            arrayOf(1,0), arrayOf(1,1),
            arrayOf(2,0), arrayOf(2,1),
            arrayOf(3,0), arrayOf(3,1),
            arrayOf(4,0), arrayOf(4,1))
        for (pos in startingPositions) {
            var isFound = true
            for (i in 0..2) {
                if (board.boardArray[pos[0]][pos[1]+i] != board.boardArray[pos[0]][pos[1]+i+1]) {
                    isFound = false
                }
            }
            if (isFound && board.boardArray[pos[0]][pos[1]] != Sign.EMPTY) {
                return board.boardArray[pos[0]][pos[1]]
            }
        }
        return Sign.EMPTY
    }

    private fun checkColumns() : Sign {
        val startingPositions = arrayOf(
            arrayOf(0,0), arrayOf(1,0),
            arrayOf(0,1), arrayOf(1,1),
            arrayOf(0,2), arrayOf(1,2),
            arrayOf(0,3), arrayOf(1,3),
            arrayOf(0,4), arrayOf(1,4))
        for (pos in startingPositions) {
            var isFound = true
            for (i in 0..2) {
                if(board.boardArray[pos[0]+i][pos[1]] != board.boardArray[pos[0]+i+1][pos[1]]) {
                    isFound = false
                }
            }
            if (isFound && board.boardArray[pos[0]][pos[1]] != Sign.EMPTY) {
                return board.boardArray[pos[0]][pos[1]]
            }
        }
        return Sign.EMPTY
    }

    private fun checkTopLeftDiagonals() : Sign {
        val startingPositions = arrayOf( arrayOf(0,0), arrayOf(1,1), arrayOf(0,1), arrayOf(1, 0))
        for (pos in startingPositions) {
            var isFound = true
            for(j in 0..2) {
                if(board.boardArray[pos[0]+j][pos[1]+j] != board.boardArray[pos[0]+j+1][pos[1]+j+1]) {
                    isFound = false
                }
            }
            if (isFound && board.boardArray[pos[0]][pos[1]] != Sign.EMPTY) {
                return board.boardArray[pos[0]][pos[1]]
            }
        }
        return Sign.EMPTY
    }

    private fun checkTopRightDiagonals() : Sign {
        val startingPositions = arrayOf( arrayOf(0,4), arrayOf(0,3), arrayOf(1,4), arrayOf(1,3))
        for (pos in startingPositions) {
            var isFound = true
            for (j in 0..2) {
                if(board.boardArray[pos[0]+j][pos[1]-j] != board.boardArray[pos[0]+j+1][pos[1]-j-1]) {
                    isFound = false
                }
            }
            if (isFound && board.boardArray[pos[0]][pos[1]] != Sign.EMPTY) {
                return board.boardArray[pos[0]][pos[1]]
            }
        }
        return Sign.EMPTY
    }
}




