package com.example.tictactoe

import java.util.*

class Bot (private var board: Board, private var mySign: Sign, private var enemySign: Sign) {

    private val boardSize = 5
    private var myMoves = ArrayList<Move>()

    fun makeMove() : Array<Int> {
        generateMoves()
        evaluateMoves()
        sortMoves()
        val bestMove = getBestMove()
        val x = bestMove.moveX
        val y = bestMove.moveY
        board.boardArray[x][y] = mySign
        return arrayOf(x, y)
    }

    private fun generateMoves() {
        myMoves = ArrayList()
        val array = board.boardArray
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                if (array[i][j] == Sign.EMPTY) {
                    val newArray = copyArray(array)
                    newArray[i][j] = mySign
                    val move = Move(newArray, mySign, enemySign, i, j)
                    myMoves.add(move)
                }
            }
        }
    }

    private fun evaluateMoves() {
        for (move in myMoves) {
            move.evaluate()
        }
    }

    private fun sortMoves() {
        myMoves.sortBy {getValue(it)}

    }

    fun getValue(move: Move) : Int {
        return move.value
    }

    private fun getBestMove() : Move {
        val bestMoveValue = myMoves[myMoves.size-1].value
        var bestMovesCounter = 0
        for (move in myMoves) {
            if (move.value == bestMoveValue) {
                bestMovesCounter++
            }
        }

        val r = Random()
        val randomNum = r.nextInt(bestMovesCounter)
        return myMoves[myMoves.size-randomNum-1]
    }


    private fun copyArray(array : Array< Array<Sign> >) : Array< Array<Sign> > {
        val newArray = Array(boardSize) { Array(boardSize) {Sign.EMPTY} }
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                newArray[i][j] = array[i][j]
            }
        }
        return newArray
    }
}