package com.example.tictactoe

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val boardSize = 5
    private var numberOfMoves = 0
    private var board = Board()
    private var game = Judge(board)
    private var isGameStarted = false
    private var isBotGame = false
    private var isBotMoving = false
    private var currentPlayer = Sign.EMPTY
    private var bot = Bot(board, Sign.EMPTY, Sign.EMPTY)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        board.initButtonsArray()
        updateBoardButtons()
    }

    fun botGameSelected(view: View) {
        board.resetBoard()
        numberOfMoves = 0
        resetImages()
        randomizePlayer()
        bot = Bot(board, getCurrentOpponent(), currentPlayer)
        isGameStarted = true
        isBotGame = true
        isBotMoving = false
        updateBoardButtons()
    }

    fun playerGameSelected(view: View) {
        board.resetBoard()
        numberOfMoves = 0
        resetImages()
        randomizePlayer()
        isGameStarted = true
        isBotGame = false
        isBotMoving = false
        updateBoardButtons()
    }

    fun gameFieldClicked(view: View) {
        numberOfMoves++
        val arr = board.getFieldCoordinates(view)
        val x = arr[0]
        val y = arr[1]
        board.boardArray[x][y] = currentPlayer
        findViewById<ImageButton>(board.buttonsArray[x][y]).setImageResource(getImage(currentPlayer))
        var winner = game.findWinner()
        if (winner != Sign.EMPTY) {
            finishGame(winner)
            return
        }
        else if (numberOfMoves == boardSize*boardSize) {
            finishGame(Sign.EMPTY)
            return
        }

        switchPlayer()
        if (isBotGame) {
            isBotMoving = true
        }
        updateBoardButtons()
        if (isBotGame) {
            val botMove = bot.makeMove()
            numberOfMoves++
            val botX = botMove[0]
            val botY = botMove[1]
            findViewById<ImageButton>(board.buttonsArray[botX][botY]).setImageResource(getImage(currentPlayer))
            isBotMoving = false
            switchPlayer()
            updateBoardButtons()
            winner = game.findWinner()
            if (winner != Sign.EMPTY) {
                finishGame(winner)
                return
            }
            else if (numberOfMoves == boardSize*boardSize) {
                finishGame(Sign.EMPTY)
                return
            }
        }
    }

    private fun updateBoardButtons() {
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                    findViewById<ImageButton>(board.buttonsArray[i][j]).isEnabled =
                        (!isBotMoving && isGameStarted && board.boardArray[i][j] == Sign.EMPTY)

            }
        }
        setPlayersMove()
    }

    private fun setPlayersMove() {
        cur_player_image.setImageResource(getImage(currentPlayer))
    }

    private fun randomizePlayer() {
        val r = Random()
        val player = r.nextInt(2)
        currentPlayer = if (player == 0) {
            Sign.CIRCLE
        } else {
            Sign.CROSS
        }
    }

    private fun switchPlayer() {
        currentPlayer = if (currentPlayer == Sign.CROSS) {
            Sign.CIRCLE
        } else {
            Sign.CROSS
        }
    }

    private fun finishGame(sign : Sign) {
        isGameStarted = true
        currentPlayer = Sign.EMPTY
        when (sign) {
            Sign.EMPTY -> info_label.text = getString(R.string.draw)
            Sign.CIRCLE -> {
                info_label.text = getString(R.string.circle_wins)
                info_label.setTextColor(Color.BLUE)
            }
            else -> {
                info_label.text = getString(R.string.cross_wins)
                info_label.setTextColor(Color.RED)
            }
        }
        updateBoardButtons()
    }

    private fun getImage(sign: Sign) : Int {
        return when (sign) {
            Sign.CROSS -> R.drawable.cross
            Sign.CIRCLE -> R.drawable.circle
            else -> R.drawable.empty
        }
    }

    private fun getCurrentOpponent() : Sign {
        return if (currentPlayer == Sign.CIRCLE) {
            Sign.CROSS
        } else {
            Sign.CIRCLE
        }
    }

    private fun resetImages() {
        info_label.text = ""
        info_label.setTextColor(Color.GRAY)
        for (i in 0 until boardSize) {
            for (j in 0 until boardSize) {
                findViewById<ImageButton>(board.buttonsArray[i][j]).setImageResource(getImage(Sign.EMPTY))
            }
        }
        cur_player_image.setImageResource(getImage(Sign.EMPTY))
    }
}
