package com.example.tictactoe

enum class Sign {
    CIRCLE, CROSS, EMPTY
}

